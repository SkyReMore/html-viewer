package com.example.Html_Viewer;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class ContentDownloader extends AsyncTask<String, Void, String> {

	TextView output;

	ContentDownloader(TextView output) {
		this.output = output;

	}

	@Override
	protected String doInBackground(String... url) {
		try {
			return getPageText(url[0]);
		} catch (IOException e) {
			return "Can't get to the page";
		}
	}

	@Override
	protected void onPostExecute(String s) {
		output.setText(s);
	}

	private String getPageText(String urlText) throws IOException {
		InputStream iStream = null;
		int inpL = 320;
		try {
			URL url = new URL(urlText);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setReadTimeout(10000);
			con.setConnectTimeout(15000);
			con.setRequestMethod("GET");
			con.setDoInput(true);
			con.connect();
			int resp = con.getResponseCode();
			Log.d("Debugging", "The response code is: " + resp);
			iStream = con.getInputStream();
			String content = readContent(iStream, inpL);
			return content;
		} finally {
			if (iStream != null)
				iStream.close();
		}
	}

	private String readContent(InputStream iStream, int inpL) throws IOException {
		Reader rdr;
		rdr = new InputStreamReader(iStream, "UTF-8");
		char[] buff = new char[inpL];
		String result = new String("");
		while(rdr.read(buff) > 0) {
			// Concatenate new text chunk to the result
			result += String.valueOf(buff);
			buff = new char[inpL];
		}
		return result;
	}
}
