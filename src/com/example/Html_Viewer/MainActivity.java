package com.example.Html_Viewer;


import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.net.URL;

public class MainActivity extends Activity {
	EditText inText;
	TextView outText;
	URL url;
	InputStream iStream;
	BufferedReader bReader;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		inText = (EditText) findViewById(R.id.editText);
		outText = (TextView) findViewById(R.id.textView);
	}

	public void fetchCode(View view) {
		String urlText = inText.getText().toString();
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni != null && ni.isConnected())
			new ContentDownloader(outText).execute(urlText);
		else
			outText.setText("Network connection isn't avaliable");
//		try {
//			url = new URL(inText.getText().toString());
//			iStream = url.openStream();
//			bReader = new BufferedReader(new InputStreamReader(iStream));
//			String line;
//			while ((line = bReader.readLine()) != null)
//				outText.append(line);
//		} catch (IOException ioexc) {
//			Log.e("URL", ioexc.getMessage());
//		} finally {
//			if (iStream != null)
//				try {
//					iStream.close();
//				} catch (IOException ioexc) {
//					Log.e("URL", ioexc.getMessage());
//				}
//		}
	}
}